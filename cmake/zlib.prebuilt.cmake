if(MSVC)
	set(SEARCH_DIR ${COCOS_EXTERNAL_DIR}/win32-specific/zlib)
endif()

find_path(ZLIB_INCLUDE_DIR zlib.h
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES include
	NO_DEFAULT_PATH
)

find_library(ZLIB_LIBRARY
	NAMES libzlib zlib1
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES prebuilt
	NO_DEFAULT_PATH
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(ZLIB REQUIRED_VARS ZLIB_LIBRARY ZLIB_INCLUDE_DIR)

if(ZLIB_FOUND)
    if(NOT TARGET ZLIB::ZLIB)
      add_library(ZLIB::ZLIB UNKNOWN IMPORTED)
      set_target_properties(ZLIB::ZLIB PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${ZLIB_INCLUDE_DIR}")

      set_property(TARGET ZLIB::ZLIB APPEND PROPERTY
        IMPORTED_LOCATION "${ZLIB_LIBRARY}")
    endif()
endif()
