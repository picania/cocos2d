if(MSVC)
	set(SEARCH_DIR ${COCOS_EXTERNAL_DIR}/curl)
endif()

find_path(CURL_INCLUDE_DIR curl/curl.h
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES include/win32
	NO_DEFAULT_PATH
)

find_library(CURL_LIBRARY
	NAMES libcurl curl
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES prebuilt/win32
	NO_DEFAULT_PATH
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(CURL REQUIRED_VARS
	CURL_INCLUDE_DIR CURL_LIBRARY
)

if(CURL_FOUND)
  if (NOT TARGET CURL::CURL)
    add_library(CURL::CURL UNKNOWN IMPORTED)
    set_target_properties(CURL::CURL PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES "${CURL_INCLUDE_DIR}")

    set_property(TARGET CURL::CURL APPEND PROPERTY IMPORTED_LOCATION "${CURL_LIBRARY}")
  endif()
endif()

mark_as_advanced(CURL_INCLUDE_DIR CURL_LIBRARY)
