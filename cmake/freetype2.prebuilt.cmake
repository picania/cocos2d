if(MSVC)
	set(SEARCH_DIR ${COCOS_EXTERNAL_DIR}/freetype2)
endif()

find_path(FREETYPE_ft2build_INCLUDE_DIR
	NAMES freetype2/ft2build.h ft2build.h
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES include/win32 include/win32/freetype2
	NO_DEFAULT_PATH
)

find_path(FREETYPE_freetype2_INCLUDE_DIR
	NAMES freetype/config/ftheader.h config/ftheader.h
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES include/win32/freetype2 include/win32
	NO_DEFAULT_PATH
)

find_library(FREETYPE_LIBRARY
	NAMES freetype
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES prebuilt/win32
	NO_DEFAULT_PATH
)

set(FREETYPE_INCLUDE_DIRS)
if(FREETYPE_ft2build_INCLUDE_DIR AND FREETYPE_freetype2_INCLUDE_DIR)
	list(APPEND FREETYPE_INCLUDE_DIRS
		${FREETYPE_ft2build_INCLUDE_DIR}
		${FREETYPE_freetype2_INCLUDE_DIR}
	)
	list(REMOVE_DUPLICATES FREETYPE_INCLUDE_DIRS)
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Freetype REQUIRED_VARS
	FREETYPE_INCLUDE_DIRS FREETYPE_LIBRARY
)

if(FREETYPE_FOUND)
  if (NOT TARGET Freetype::freetype)
    add_library(Freetype::freetype UNKNOWN IMPORTED)
    set_target_properties(Freetype::freetype PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES "${FREETYPE_INCLUDE_DIRS}")

    set_property(TARGET Freetype::freetype APPEND PROPERTY IMPORTED_LOCATION "${FREETYPE_LIBRARY}")
  endif()
endif()

mark_as_advanced(FREETYPE_INCLUDE_DIRS PNG_LIBRARY)
