if(MSVC)
	set(SEARCH_DIR ${COCOS_EXTERNAL_DIR}/websockets)
endif()

find_path(WEBSOCKETS_INCLUDE_DIR libwebsockets.h
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES include/win32
	NO_DEFAULT_PATH
)

find_library(WEBSOCKETS_LIBRARY
	NAMES websockets libwebsockets
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES prebuilt/win32
	NO_DEFAULT_PATH
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(WebSockets REQUIRED_VARS
	WEBSOCKETS_INCLUDE_DIR WEBSOCKETS_LIBRARY
)

if(WEBSOCKETS_FOUND)
  if (NOT TARGET WebSockets::WebSockets)
    add_library(WebSockets::WebSockets UNKNOWN IMPORTED)
    set_target_properties(WebSockets::WebSockets PROPERTIES
		INTERFACE_INCLUDE_DIRECTORIES "${WEBSOCKETS_INCLUDE_DIR}")

    set_property(TARGET WebSockets::WebSockets APPEND PROPERTY
		IMPORTED_LOCATION "${WEBSOCKETS_LIBRARY}"
	)
  endif()
endif()

mark_as_advanced(WEBSOCKETS_INCLUDE_DIR WEBSOCKETS_LIBRARY)
