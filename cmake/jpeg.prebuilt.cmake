if(MSVC)
	set(SEARCH_DIR ${COCOS_EXTERNAL_DIR}/jpeg)
endif()

find_path(JPEG_INCLUDE_DIR jpeglib.h
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES include/win32
	NO_DEFAULT_PATH
)

find_library(JPEG_LIBRARY
	NAMES jpeg libjpeg
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES prebuilt/win32
	NO_DEFAULT_PATH
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(jpeg REQUIRED_VARS
	JPEG_INCLUDE_DIR JPEG_LIBRARY
)

if(JPEG_FOUND)
  if (NOT TARGET JPEG::JPEG)
    add_library(JPEG::JPEG UNKNOWN IMPORTED)
    set_target_properties(JPEG::JPEG PROPERTIES
		INTERFACE_INCLUDE_DIRECTORIES "${JPEG_INCLUDE_DIR}")

    set_property(TARGET JPEG::JPEG APPEND PROPERTY
		IMPORTED_LOCATION "${JPEG_LIBRARY}"
	)
  endif()
endif()

mark_as_advanced(JPEG_INCLUDE_DIR JPEG_LIBRARY)
