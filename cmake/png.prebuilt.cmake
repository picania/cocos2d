if(MSVC)
	set(SEARCH_DIR ${COCOS_EXTERNAL_DIR}/png)
endif()

find_path(PNG_INCLUDE_DIR png.h
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES include/win32
	NO_DEFAULT_PATH
)

find_library(PNG_LIBRARY
	NAMES libpng png
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES prebuilt/win32
	NO_DEFAULT_PATH
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(PNG REQUIRED_VARS
	PNG_INCLUDE_DIR PNG_LIBRARY
)

if(PNG_FOUND)
  if (NOT TARGET PNG::PNG)
    add_library(PNG::PNG UNKNOWN IMPORTED)
    set_target_properties(PNG::PNG PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES "${PNG_INCLUDE_DIR}")

    set_property(TARGET PNG::PNG APPEND PROPERTY IMPORTED_LOCATION "${PNG_LIBRARY}")
  endif()
endif()

mark_as_advanced(PNG_INCLUDE_DIR PNG_LIBRARY)
