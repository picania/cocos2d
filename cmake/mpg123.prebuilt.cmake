if(MSVC)
	set(SEARCH_DIR ${COCOS_EXTERNAL_DIR}/win32-specific/MP3Decoder)
endif()

find_path(MPG123_INCLUDE_DIR mpg123.h
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES include
	NO_DEFAULT_PATH
)

find_library(MPG123_LIBRARY
	NAMES libmpg123
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES prebuilt
	NO_DEFAULT_PATH
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(MPG123 REQUIRED_VARS
	MPG123_INCLUDE_DIR MPG123_LIBRARY
)

if(MPG123_FOUND)
  if (NOT TARGET MP3::mp3)
    add_library(MP3::mp3 UNKNOWN IMPORTED)
    set_target_properties(MP3::mp3 PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES "${MPG123_INCLUDE_DIR}")

    set_property(TARGET MP3::mp3 APPEND PROPERTY IMPORTED_LOCATION "${MPG123_LIBRARY}")
  endif()
endif()

mark_as_advanced(MPG123_INCLUDE_DIR MPG123_LIBRARY)
