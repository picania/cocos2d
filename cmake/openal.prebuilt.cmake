if(MSVC)
	set(SEARCH_DIR ${COCOS_EXTERNAL_DIR}/win32-specific/OpenalSoft)
endif()

find_path(OPENAL_INCLUDE_DIR AL/al.h
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES include
	NO_DEFAULT_PATH
)

find_library(OPENAL_LIBRARY
	NAMES OpenAL32
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES prebuilt
	NO_DEFAULT_PATH
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(OpenAL REQUIRED_VARS
	OPENAL_INCLUDE_DIR OPENAL_LIBRARY
)

if(OPENAL_FOUND)
  if (NOT TARGET OpenAL::OpenAL)
    add_library(OpenAL::OpenAL UNKNOWN IMPORTED)
    set_target_properties(OpenAL::OpenAL PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES "${OPENAL_INCLUDE_DIR}")

    set_property(TARGET OpenAL::OpenAL APPEND PROPERTY IMPORTED_LOCATION "${OPENAL_LIBRARY}")
  endif()
endif()

mark_as_advanced(OPENAL_INCLUDE_DIR OPENAL_LIBRARY)
