add_library(Msvc::system_dependencies INTERFACE IMPORTED)
target_link_libraries(Msvc::system_dependencies INTERFACE
	$<$<CXX_COMPILER_ID:MSVC>:opengl32.lib>
	$<$<CXX_COMPILER_ID:MSVC>:glu32.lib>
	$<$<CXX_COMPILER_ID:MSVC>:wininet.lib>
	$<$<CXX_COMPILER_ID:MSVC>:version.lib>
	$<$<CXX_COMPILER_ID:MSVC>:ws2_32.lib>
	$<$<CXX_COMPILER_ID:MSVC>:winmm.lib>
)
