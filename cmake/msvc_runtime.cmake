add_library(Msvc::static_runtime INTERFACE IMPORTED)
target_compile_options(Msvc::static_runtime INTERFACE
	$<$<AND:$<CXX_COMPILER_ID:MSVC>,$<CONFIG:Debug>>:/MTd>
	$<$<AND:$<CXX_COMPILER_ID:MSVC>,$<CONFIG:Release>>:/MT>
    $<$<AND:$<CXX_COMPILER_ID:MSVC>,$<CONFIG:MinSizeRel>>:/MT>
	$<$<AND:$<CXX_COMPILER_ID:MSVC>,$<CONFIG:RelWithDebInfo>>:/MT>
	$<$<CXX_COMPILER_ID:MSVC>:/MP>
)

add_library(Msvc::dynamic_runtime INTERFACE IMPORTED)
target_compile_options(Msvc::dynamic_runtime INTERFACE
	$<$<AND:$<CXX_COMPILER_ID:MSVC>,$<CONFIG:Debug>>:/MDd>
	$<$<AND:$<CXX_COMPILER_ID:MSVC>,$<CONFIG:Release>>:/MD>
    $<$<AND:$<CXX_COMPILER_ID:MSVC>,$<CONFIG:MinSizeRel>>:/MD>
	$<$<AND:$<CXX_COMPILER_ID:MSVC>,$<CONFIG:RelWithDebInfo>>:/MD>
	$<$<CXX_COMPILER_ID:MSVC>:/MP>
)
