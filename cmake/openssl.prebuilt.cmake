if(MSVC)
	set(SEARCH_DIR ${COCOS_EXTERNAL_DIR}/openssl)
endif()

find_path(OPENSSL_INCLUDE_DIR openssl/ssl.h
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES include/win32
	NO_DEFAULT_PATH
)

find_library(OPENSSL_SSL_LIBRARY
	NAMES libssl ssleay32 ssl
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES prebuilt/win32
	NO_DEFAULT_PATH
)

find_library(OPENSSL_CRYPTO_LIBRARY
	NAMES libcrypto libeay32 crypto
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES prebuilt/win32
	NO_DEFAULT_PATH
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(OpenSSL REQUIRED_VARS
	OPENSSL_INCLUDE_DIR OPENSSL_SSL_LIBRARY OPENSSL_CRYPTO_LIBRARY
)

if(OPENSSL_FOUND)
	if (NOT TARGET OpenSSL::SSL)
		add_library(OpenSSL::SSL UNKNOWN IMPORTED)
		set_target_properties(OpenSSL::SSL PROPERTIES
			INTERFACE_INCLUDE_DIRECTORIES "${OPENSSL_INCLUDE_DIR}")

		set_property(TARGET OpenSSL::SSL APPEND PROPERTY
			IMPORTED_LOCATION "${OPENSSL_SSL_LIBRARY}"
		)
	endif()

	if (NOT TARGET OpenSSL::Crypto)
		add_library(OpenSSL::Crypto UNKNOWN IMPORTED)
		set_target_properties(OpenSSL::Crypto PROPERTIES
			INTERFACE_INCLUDE_DIRECTORIES "${OPENSSL_INCLUDE_DIR}")

		set_property(TARGET OpenSSL::Crypto APPEND PROPERTY
			IMPORTED_LOCATION "${OPENSSL_CRYPTO_LIBRARY}"
		)
	endif()
endif()

mark_as_advanced(OPENSSL_INCLUDE_DIR OPENSSL_SSL_LIBRARY OPENSSL_CRYPTO_LIBRARY)
