if(MSVC)
	set(SEARCH_DIR ${COCOS_EXTERNAL_DIR}/tiff)
endif()

find_path(TIFF_INCLUDE_DIR tiff.h
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES include/win32
	NO_DEFAULT_PATH
)

find_library(TIFF_LIBRARY
	NAMES libtiff tiff
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES prebuilt/win32
	NO_DEFAULT_PATH
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(TIFF REQUIRED_VARS
	TIFF_INCLUDE_DIR TIFF_LIBRARY
)

if(TIFF_FOUND)
  if (NOT TARGET TIFF::TIFF)
    add_library(TIFF::TIFF UNKNOWN IMPORTED)
    set_target_properties(TIFF::TIFF PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES "${TIFF_INCLUDE_DIR}")

    set_property(TARGET TIFF::TIFF APPEND PROPERTY IMPORTED_LOCATION "${TIFF_LIBRARY}")
  endif()
endif()

mark_as_advanced(TIFF_INCLUDE_DIR TIFF_LIBRARY)
