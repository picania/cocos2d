if(MSVC)
	set(SEARCH_DIR ${COCOS_EXTERNAL_DIR}/win32-specific/OggDecoder)
endif()

find_path(OGG_INCLUDE_DIR ogg/ogg.h
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES include
	NO_DEFAULT_PATH
)

find_path(VORBIS_INCLUDE_DIR vorbis/codec.h
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES include
	NO_DEFAULT_PATH
)

find_library(OGG_LIBRARY
	NAMES ogg libogg
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES prebuilt
	NO_DEFAULT_PATH
)

find_library(VORBIS_LIBRARY
	NAMES vorbis libvorbis
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES prebuilt
	NO_DEFAULT_PATH
)

find_library(VORBISFILE_LIBRARY
	NAMES vorbisfile libvorbisfile
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES prebuilt
	NO_DEFAULT_PATH
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Vorbis REQUIRED_VARS
	OGG_INCLUDE_DIR VORBIS_INCLUDE_DIR
	OGG_LIBRARY VORBIS_LIBRARY VORBISFILE_LIBRARY
)

if(VORBIS_FOUND)
    if(NOT TARGET Vorbis::vorbis)
      add_library(Vorbis::vorbis UNKNOWN IMPORTED)
      set_target_properties(Vorbis::vorbis PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${VORBIS_INCLUDE_DIR}")

      set_property(TARGET Vorbis::vorbis APPEND PROPERTY
        IMPORTED_LOCATION "${VORBIS_LIBRARY}")
    endif()

	if(NOT TARGET Vorbis::vorbisfile)
      add_library(Vorbis::vorbisfile UNKNOWN IMPORTED)
      set_target_properties(Vorbis::vorbisfile PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${VORBIS_INCLUDE_DIR}")

      set_property(TARGET Vorbis::vorbisfile APPEND PROPERTY
        IMPORTED_LOCATION "${VORBISFILE_LIBRARY}")
    endif()

	if(NOT TARGET Vorbis::ogg)
      add_library(Vorbis::ogg UNKNOWN IMPORTED)
      set_target_properties(Vorbis::ogg PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${OGG_INCLUDE_DIR}")

      set_property(TARGET Vorbis::ogg APPEND PROPERTY
        IMPORTED_LOCATION "${OGG_LIBRARY}")
    endif()
endif()

mark_as_advanced(
	OGG_INCLUDE_DIR VORBIS_INCLUDE_DIR
	OGG_LIBRARY VORBIS_LIBRARY VORBISFILE_LIBRARY
)
