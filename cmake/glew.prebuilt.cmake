if(MSVC)
	set(SEARCH_DIR ${COCOS_EXTERNAL_DIR}/win32-specific/gles)
endif()

find_path(GLEW_INCLUDE_DIR GL/glew.h
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES include include/OGLES
	NO_DEFAULT_PATH
)

find_library(GLEW_LIBRARY
	NAMES glew32
	PATHS ${SEARCH_DIR}
	PATH_SUFFIXES prebuilt
	NO_DEFAULT_PATH
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(GLEW REQUIRED_VARS GLEW_LIBRARY GLEW_INCLUDE_DIR)

if(GLEW_FOUND)
  if (NOT TARGET GLEW::GLEW)
    add_library(GLEW::GLEW UNKNOWN IMPORTED)
    set_target_properties(GLEW::GLEW PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES "${GLEW_INCLUDE_DIR}")

    set_property(TARGET GLEW::GLEW APPEND PROPERTY IMPORTED_LOCATION "${GLEW_LIBRARY}")
  endif()
endif()

mark_as_advanced(GLEW_INCLUDE_DIR GLEW_LIBRARY)
