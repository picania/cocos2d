set(COCOS_PHYSICS_SRC
    physics/CCPhysicsBody.cpp
    physics/CCPhysicsContact.cpp
    physics/CCPhysicsJoint.cpp
    physics/CCPhysicsShape.cpp
    physics/CCPhysicsWorld.cpp
)
source_group(src\\physics FILES ${COCOS_PHYSICS_SRC})
