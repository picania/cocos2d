set(COCOS_VR_SRC
    vr/CCVRDistortion.cpp
    vr/CCVRDistortionMesh.cpp
    vr/CCVRGenericRenderer.cpp
    vr/CCVRGenericHeadTracker.cpp
)
source_group(src\\vr FILES ${COCOS_VR_SRC})
