
set(COCOS_NAVMESH_SRC
    navmesh/CCNavMesh.cpp
    navmesh/CCNavMeshAgent.cpp
    navmesh/CCNavMeshDebugDraw.cpp
    navmesh/CCNavMeshObstacle.cpp
    navmesh/CCNavMeshUtils.cpp
)
source_group(src\\navmesh FILES ${COCOS_NAVMESH_SRC})
